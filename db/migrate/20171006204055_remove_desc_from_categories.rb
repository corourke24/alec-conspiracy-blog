class RemoveDescFromCategories < ActiveRecord::Migration[5.0]
  def change
    remove_column :categories, :desc, :text
  end
end
