class CategoriesController < ApplicationController
  before_action :set_category, only: [:show, :edit, :update, :destroy]
  
  # GET /categories
  # GET /categories.json
  def index
    @categories = Category.all.order(created_at: :desc).paginate(page: params[:page], :per_page => 10)
  end

  # GET /categories/1
  # GET /categories/1.json
  def show
  end

  # GET /categories/new
  def new
    @category = Category.new
  end

  # GET /categories/1/edit
  def edit
  end
  
  def create
    @category = Category.new(category_params)
    if @category.save(category_params)
      flash[:notice] = "Successfully created category!"
      redirect_back(fallback_location: @category)
    else
      flash[:alert] = "Error creating new category!"
      render :new
    end
  end

  # PATCH/PUT /categories/1
  # PATCH/PUT /categories/1.json
  def update
    respond_to do |format|
      if @category.update(category_params)
        format.html { redirect_to @category, notice: 'Category was successfully updated.' }
        format.json { render :show, status: :ok, location: @category }
      else
        format.html { render :edit }
        format.json { render json: @category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /categories/1
  # DELETE /categories/1.json
  def destroy
    if deletable?
      cat_change
      @category.destroy
      respond_to do |format|
        format.html { redirect_to categories_url, notice: 'Category was successfully destroyed.' }
        format.json { head :no_content }
      end
    else
      redirect_to categories_url, notice: 'Uncategorized Category can not be deleted'
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_category
      @category = Category.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def category_params
      params.require(:category).permit(:name)
    end
    
    def cat_change
        @posts = Post.all
        @posts.each do |post|
        if post.category_id = @category.id
            post.update_attribute(:category_id, 1)
        end
      end
    end
    
    # Make Uncategorized undeletable
    def deletable?
      @category.id != 1
    end
  
end
